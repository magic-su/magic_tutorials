# Here we have few tutorials demonstrating use-cases of MagiC. #
All tutorials are covering all stages starting from all-atom MD and up to small-size coarse-grained MD (reproduction run).
So far we have:
---
## Basic tutorials

* **NaCl**: Tutorial demonstrating basic usage of MagiC on simple system of 20 Na+ and 20 Cl- dissolved in water

* **DMPC-Cholesterol**: Complete tutorial showing how to get CG-model for mixture of phospholipids and cholesterol.

---

## Advanced topics

* **Peptide chains with Mg2+**: Example how to have same bond-type shared among few molecular types

* **1DNA-K** : How to have non-standard exclusions for NB-interactions.


---

## Requirements

#### Software requirements
* MagiC ver>3.0 :)
* Python libraries: numpy, scipy, pandas, matplotlib, MDAnalysis.
* Jupyter notebook 
* VMD
* MD engines: GROMACS, LAMMPS, GALAMOST (at least one of them)


#### User requirements

We assume that the user know following subjects:

* Underlying theory of Molecular Simulations (both MD and MC)
* Practical experience of using one of the 3 MD codes (GROMACS, LAMMPS, GALAMOST)
* Basic knowledge of Python syntax
* Command-line tools of UNIX

* Some skill in telepathy is a merit, since not all of the tutorials are well-written, so you will need to guess what we had meant.

