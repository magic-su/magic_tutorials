{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MagiC tutorial: \n",
    "## Computation of solvent-mediated effective potentials between Na+ and Cl- ions in water\n",
    "\n",
    "** Alexander Lyubartsev(1), Alexander Mirzoev(2) **\n",
    "\n",
    "(1): Department of Materials and Environmental Chemistry, Stockholm University, Stockholm, Sweden\n",
    "(2): School of Biological Sciences, Nanyang Technological University, Singapore\n",
    "\n",
    "November 2017\n",
    "\n",
    "---\n",
    "This tutorial illustrate example of computation of effective potentials for ions in NaCl solution. This example follows these works:\n",
    "* A.Lyubartsev, A.Laaksonen, \"Calculation of Effective Interaction Potentials from Radial Distribution Functions: A Reverse Monte Carlo Approach\", Phys.Rev.E, 52, 3730 (1995)\n",
    "* A.Mirzoev, A.P.Lyubartsev, \"Effective solvent-mediated interaction potentials of Na+ and Cl- in aqueous solution: temperature dependence\", PCCP, 13, 5722 (2011)\n",
    "\n",
    "\n",
    "In the example, radial distribution functions (RDF) between ions in water solutions, computed by atomistic molecular dynamics, are used to compute effective solvent-mediated potentials, which, in simulation of ions without explicit water, reproduce the same RDF as those obtained in the atomistic simulations.\n",
    "The example contains 4 folders in which input and output files relevant for each step of computations of the effective potentials are collected:\n",
    "* 1 - High-resolution (atomistic) molecular dynamics ( 1-HighResolutionMD )\n",
    "* 2 - Coarse-graining of the trajectory ( 2-CGtraj )\n",
    "* 3 - Computation of the radial distribution functions from the coarse-grained\n",
    "trajectory ( 3-RDF )\n",
    "* 4 - Computations of the effective potentials ( 4-IMC )\n",
    "Additionally, folder Input-only contains the same 4 subfolders with only input files. It can be\n",
    "advisable to run test study in this folder and then compare with the reference output files.\n",
    "It is assumed that MagiC package (v. >= 2.4) is compiled and installed.\n",
    "* 5, 6, 7 - CGMD-GROMACS, CGMD-LAMMPS, CGMD-GALAMOST \n",
    "Export of the topology and potentials to each of three supported MD-engines and simple CGMD calculations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Import MagicTools Library and set location for the tutorial root-folder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import MagicTools as MT\n",
    "HOME = ! pwd\n",
    "HOME = HOME[0]\n",
    "HOME"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step1: High-resolution (atomistic) molecular dynamics simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 1-HighResolutionMD/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This directory contains setup of molecular dynamics simulations of 20 NaCl ion pairs (modelled by Smith-Dang potentials) dissolved in 1000 water molecules (flexible SPC model) using MDynaMix software <http://www.fos.su.se/~sasha/mdynamix>.\n",
    "\n",
    "Files:\n",
    "* md.input : main input file for MDynaMix (12 ns for 20 ion pairs in 1024 water of which 2 ns are left for equilibration)\n",
    "* H2O.mmol, Na+_SD.mmol, Cl-_SD.mmol - molecular topology (structure and ff-parameters) files for water and ions\n",
    "output: trajectory files obtained by running Mdymamix\n",
    "\n",
    "Run MD simulation (implies you have MDynaMix ver > 5.2.8)\n",
    "> ./md md.input > md.output\n",
    "\n",
    "or in case of parallel execution\n",
    "\n",
    "> mpirun -np xx ./mdp md.input > md.output\n",
    "\n",
    "with XX giving the number of available cores/processors\n",
    "\n",
    "\n",
    "The simulation generates 12 trajectory files 1 ns each with names *\"nacl-1M.001\",..., \"nacl-1M.012\"*\n",
    "in XMOL format. To save space, only ions are included into trajectory. These files (except .001 and .\n",
    "002 covering equilibration stage) are already included in the Example (see subfolder result) and can be used directly in step 2 without running atomistic simulations. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Converting atomistic trajectory to a coarse-grained trajectory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd {HOME}/2-CGtraj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Input files: \n",
    "* cgtraj_NaCl.inp : main input file\n",
    "* nacl-1M.0xx from step 1: atomistic trajectory files (shall be copied from step 1)\n",
    "* Na+_SD.mmol, Cl-_SD.mmol - Molecular topology files (shall be copied from step 1)\n",
    "\n",
    "Run the bead mapping:\n",
    "> cgtraj cgtraj_NaCl.inp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# or execute this cell\n",
    "! cgtraj cgtraj_NaCl.inp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It will result in output files:\n",
    "* _cgtraj.NaCl.xmol_ - bead-mapped trajectory (= coordinates of Na+ and Cl- ions only) in .xmol format\n",
    "* Cl-.CG.mmol  Na+.CG.mmol - CG-molecular representation (without CG types and bonds) for the involved molecules\n",
    "\n",
    "*Note 1.* Because of simplicity of the example, and because water coordinates were already excluded from the atomistic trajectory, the coarse-grained trajectory contains the same ion coordinates as the atomistic one.\n",
    "\n",
    "*Note 2.* An alternative version of the cgtraj input file _cgtraj_NaCl-alt.inp_ is also present. It does not require atomistic .mmol files inherited from MDynaMix program. In this case the .CG.mmol files are generated with default masses of the CG sites equal to 1 and default charges equal to 0, these need to be set to real values manually."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Compute RDFs from the coarse-grained trajectory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "** Input:**\n",
    "* rdf.inp - main input file\n",
    "* ../2-CGtraj/cgtraj.NaCl.xmol - coarse-grained trajectory created at the previous step\n",
    "\n",
    "** Output:**\n",
    "* NaCl_1M.rdf - files with radial distribution functions in MagiC format\n",
    "* Na+_SD.CG.mcm\n",
    "* Cl-_SD.CG.mcm - topology files for coarse-grained molecules\n",
    "* exclusions.dat - file specifying exclusions from non-bonded interactions, based on bonds between atoms. As we do not have any bonds, no exclusions shall be generated.\n",
    "\n",
    "Enter directory 3-RDF. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd {HOME}/3-RDF"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run python script which computes RDFs:\n",
    "\n",
    "> rdf.py -i rdf.inp\n",
    "\n",
    "or for parallel run (-np specifies number of cores to run)\n",
    "\n",
    "> rdf.py -i rdf.inp -np 6\n",
    "\n",
    "This results in file *NaCl_1M.rdf* which contains Na-Na, Na-Cl and Cl-Cl RDFs in format suitable for MagiC. Also, topology files *Na+_SD.CG.mcm* and *Cl-_SD.CG.mcm* for the next step are created. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 4  Inverse MC - the main MagiC module.\n",
    "\n",
    "** Input: **\n",
    "* NaCl-rdf.rdf - file with reference RDFs\n",
    "* Na+_SD.CG.mcm\n",
    "* Cl-_SD.CG.mcm - molecular description files for MagiC\n",
    "* magic_NaCl.inp1, magic_NaCl.inp2 - input files for two steps of the inverse procedure\n",
    "\n",
    "#### Running the calculation\n",
    "\n",
    "** Stage 1 **\n",
    "> magic-gfortran Magic_NaCl.inp1 > NaCl.out1\n",
    "\n",
    "or (implies 8 processors or cores) :\n",
    "\n",
    "> mpirun -np 8 magic-gfortran-mpi Magic_NaCl.inp1 > NaCl.out1 \n",
    "\n",
    "This will run 10 iterations of the inverse Monte Carlo, 11000000 steps each,\n",
    "with regularization parameter 0.2, starting from zero potential. This creates,\n",
    "after each iteration, a new potential file (01.NaCl.i001.pot - 01.NaCl.i010.pot)\n",
    "and log output from each core NaCl.01.p001 - NaCl.01.p008 (only in case of parallel execution)\n",
    "The main output file magic_NaCl.out1 contains log of the calculation.\n",
    "\n",
    "** Stage 2 **\n",
    "\n",
    "> magic-gfortran magic_NaCl.inp2 > magic_NaCl.out2 \n",
    "\n",
    "This will run a second series of 10 iterations of the inverse Monte Carlo.\n",
    "Note the changes from the first series of iterations:\n",
    "* `InputPot = 'NaCl.01.i010.pot'` the program now starts from the last potential of the previous series ( InputPot parameter uncommented):\n",
    "* `MCSteps = 20000000`, - the number of MC steps is increased to 20 000 000 \n",
    "* `MCStepsEquil = 4000000`  - and for equilibration to 4 000 000\n",
    "* `REGP = 0.5`, -the regularization parameter is increased to 0.5\n",
    "* `Output = NaCl.02` - the Base output filename is changed, in order to not overwrite result of the first series\n",
    "\n",
    "As before, the program creates after each iteration a new potential file\n",
    "(02.NaCl.i001.pot - 02.NaCl.i010.pot).\n",
    "The main output file magic_NaCl.out2 contains log of the calculation.\n",
    "\n",
    "** Note.**\n",
    "The number of MC steps is given per processor, and the quality of computations/noise will depend on\n",
    "the number of processor used. The output files of the example are computed at 8 processors. If you use\n",
    "smaller number of processor, it is advisable to increase the number of steps accordingly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analysis of the IMC calculation results ###\n",
    "\n",
    "Now let's take a look on the outcome of the calculation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd {HOME}/4-IMC"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, make a brief check of the convergence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.Deviation(['NaCl_IMC.out1', 'NaCl_IMC.out2'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then let's see how the sampled RDFs are actually look like, compared to the reference:\n",
    "\n",
    "Read the reference rdfs form the .rdf-file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdfset_reference = MT.ReadRDF('NaCl_1M.rdf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you can read the reference RDFs from IMC-output file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdfset_reference_fromIMC = MT.ReadMagiC('NaCl_IMC.out1', DFType='RDFref')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read RDFs sampled during the first and the second stages of the IMC. I only read every second iteration, to make the plots less messy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdfsets_IMC_1 = MT.ReadMagiC('NaCl_IMC.out1', DFType='RDF', iters=(1,3,5,7,10))\n",
    "rdfsets_IMC_2 = MT.ReadMagiC('NaCl_IMC.out2', DFType='RDF', iters=(1,3,5,7,10))\n",
    "\n",
    "# or you can read them at once: \n",
    "rdfsets_IMC_all = MT.ReadMagiC(['NaCl_IMC.out1','NaCl_IMC.out2'], DFType='RDF')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NB!** Please note, that Reference RDFs `rdfset_reference` and `rdfset_reference_fromIMC` are a single RDFset-objects, while `rdfsets_IMC_1` and `rdfsets_IMC_2` are lists of RDFset-objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's set some plot-related properties, to make the plots look nicer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# make reference RDFs bold\n",
    "rdfset_reference.SetPlotProperty('linewidth', 3)\n",
    "# make RDFs from the first stage dashed\n",
    "for rdfset_ in rdfsets_IMC_1:\n",
    "    rdfset_.SetPlotProperty('linestyle','--')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finaly put them on a plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "MT.MultPlot([rdfset_reference] + rdfsets_IMC_1 + rdfsets_IMC_2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "** Syntax-related comment:** MultPlot requier a list_of_DFset as an input. So I have constructed it in a following way:\n",
    "\n",
    "`[ rdfs_reference ]` - made a list with a single object `rdfs_reference`\n",
    "\n",
    "`+ rdfsets_IMC_1` - then merged with `rdfsets_IMC_1`, which is a list of RDFsets, \n",
    "\n",
    "`+ rdfsets_IMC_2` - then finally merged it with `rdfsets_IMC_2`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, already after 20 iterations the RDFs are almost undistinguishable from the reference.\n",
    "\n",
    "Now let's read and plot the resulting effective potentials: `NaCl.02.i010.pot`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pot = MT.ReadPot('NaCl.02.i010.pot')\n",
    "pot.Plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or to plot it on one figure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.OnePlot(pot)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Show total potentials with addition of the Coulombic part:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "TotalPots=MT.TotalPots(pot, eps=78.0, mcmfile=['Na+_SD.CG.mcm','Cl-_SD.CG.mcm'])\n",
    "MT.MultPlot([pot, TotalPots])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also interesting to see how the potential corrections looks during IMC-iteration. To avoid overloading of the plots we will only import every second iteration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pot_corr_IMC_all = MT.ReadMagiC(['NaCl_IMC.out1','NaCl_IMC.out2'], DFType='PotCorr',\n",
    "                                iters=(1,3,5,7,10), quiet=True)\n",
    "\n",
    "# Make first 5 sets (from the first stage) dashed:\n",
    "for pot_ in pot_corr_IMC_all[0:5]:\n",
    "    pot_.SetPlotProperty('linestyle','--')\n",
    "# Plot\n",
    "MT.MultPlot(pot_corr_IMC_all)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trend is quite clear - when RDFs are reaching convergence, the potential update is getting closer to zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5. CGMD - GROMACS\n",
    "Let's test our CG-model by running it in the same conditions (box-size, composition, temperature, etc.) as original AAMD simulation and then compare RDFs sampled in such CGMD run with original reference RDFs. We usually call this \"Reproduction test\"\n",
    "\n",
    "Required files: \n",
    "* NaCl.02.i010.pot - Resulting effective potentials from the IMC \n",
    "* Na.CG.mcm, Cl.CG.mcm - molecular topology, same as in IMC, just renamed to be short\n",
    "\n",
    "* start.xmol - starting geometry for the CGMD simulation\n",
    "* rdf.repro.inp - input file for reproduction RDF calculation\n",
    "* 01.MD.mdp - simulation settings for GROMACS"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd {HOME}/5-CGMD-GROMACS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Export topology to GROMACS ####"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.GromacsTopology(geometry='start.xmol',\n",
    "                   NMolMType=[20,20], \n",
    "                   mcmfile=['Na.CG.mcm','Cl.CG.mcm'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will result in two new files: \n",
    "* topol.top - topology of the system\n",
    "* start.gro - starting coordinates for the system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to create an index-file for the system:\n",
    "In the terminal run \n",
    "> gmx make_ndx -f start.gro\n",
    "\n",
    "Since the system is quite simple all index-groups are autodetected. You just need to save the file by pressing **q**\n",
    "\n",
    "this will result in the new file: **index.ndx**\n",
    "\n",
    "Now we can compile binary setting file for the simulation:\n",
    "\n",
    "> gmx grompp -n index.ndx -p topol.top -f 01.MD.mdp -c start.gro -maxwarn 1 -o 01.MD.tpr\n",
    "\n",
    "and get the resulting **01.MD.tpr**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Export potentials to GROMACS ####"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pot = MT.ReadPot('NaCl.02.i010.pot', quiet=True)\n",
    "MT.PotsExport2Gromacs(pot, npoints=2000, Umax=10000, Rmaxtable=2.0, \n",
    "                     method='gauss', sigma=0.55, zeroforce=True,\n",
    "                    hardcopy=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The only remaning thing is to run Gromacs:\n",
    "> gmx mdrun -s 01.MD.tpr -x 01.MD.xtc -e 01.MD.edr -g 01.MD.log -v\n",
    "\n",
    "When it finishes, we can calculate RDFs based on the `01.MD.xtc` trajectory file:\n",
    "\n",
    "> rdf.py -i rdf.inp -np 5\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rdf_repro_gromacs = MT.ReadRDF('NaCl_repro_gromacs.rdf', quiet=True)\n",
    "rdf_ref = MT.ReadRDF('NaCl_1M.rdf',quiet=True)\n",
    "MT.MultPlot([rdf_repro_gromacs, rdf_ref])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 6. Coarse Grain MD - LAMMPS\n",
    "Let's run the reproduction test with LAMMPS\n",
    "Required files: \n",
    "* NaCl.02.i010.pot - Resulting effective potentials from the IMC \n",
    "* Na.CG.mcm, Cl.CG.mcm - molecular topology, same as in IMC, just renamed to be short\n",
    "* start.xmol - starting geometry for the CGMD simulation\n",
    "* LAMMPS.run.inp - CGMD-simulation settings\n",
    "* rdf.repro.inp - input file for reproduction RDF calculation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd {HOME}/6-CGMD-LAMMPS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Export potentials to LAMMPS ####\n",
    "The export is quite similar to what we did for GROMACS"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pot = MT.ReadPot('NaCl.02.i010.pot', quiet=True)\n",
    "MT.PotsExport2LAMMPS(pot, npoints=1500, Umax=10000, Rmaxtable=15.0, \n",
    "                     method='gauss', sigma=0.55,\n",
    "                    hardcopy=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Export topology to LAMMPS ####"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.LAMMPSTopology(xmol='start.xmol',NMolMType=[20,20], \n",
    "                   mcmfile=['Na.CG.mcm','Cl.CG.mcm'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the result, we got two files:\n",
    "* LAMMPS.data - topology of the system. \n",
    "* LAMMPS.data.inc - interaction specification and potential table binding.\n",
    "\n",
    "Now we are ready to run LAMMPS\n",
    "\n",
    "> mpirun -np 5 lmp_mpi < LAMMPS.run.inp\n",
    "\n",
    "When it finishes, we can calculate RDFs based on the `trj.xtc` trajectory file:\n",
    "\n",
    "> rdf.py -i rdf.inp -np 5\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compare RDFs sampled in LAMMPS and GROMACS"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rdf_repro_gromacs = MT.ReadRDF('../5-CGMD-GROMACS/NaCl_repro_gromacs.rdf', quiet=True)\n",
    "rdf_repro_lammps = MT.ReadRDF('NaCl_repro_lammps.rdf', quiet=True)\n",
    "rdf_ref = MT.ReadRDF('NaCl_1M.rdf',quiet=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.MultPlot([rdf_repro_gromacs, rdf_repro_lammps, rdf_ref])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, we have fairly nice agreement between the RDFs, congratulations!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 7. Coarse Grain MD - GALAMOST\n",
    "Let's run the reproduction test with GALAMOST\n",
    "Required files: \n",
    "* NaCl.02.i010.pot - Resulting effective potentials from the IMC \n",
    "* Na.CG.mcm, Cl.CG.mcm - molecular topology, same as in IMC, just renamed to be short\n",
    "* start.xmol - starting geometry for the CGMD simulation\n",
    "* galajob.gala - CGMD-simulation run-script\n",
    "* rdf.repro.inp - input file for reproduction RDF calculation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd {HOME}/7-CGMD-GALAMOST/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Reading and exporting the potential"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pot = MT.ReadPot('NaCl.02.i010.pot')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "GALAMOST requres Rcut to be shorter that 1/3 of the box size, so we need to truncate the potentials"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pot.CutTail(9.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Export the potentials"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.PotsExport2GALAMOST(pot, npoints=900, Umax=10000, Rmaxtable=0.9, sigma=0.55)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Export topology of the system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.GALAMOSTTopology(80.0, NMolMType=[20,20], \n",
    "                   mcmfile=['Na.CG.mcm','Cl.CG.mcm'],\n",
    "                   xmol='start.xmol')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Run the simulation\n",
    "\n",
    "> python galajob.gala\n",
    "\n",
    "once it finishes, we can convert the resulting trajectory from dcd to xtc format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "MT.dcd2xtc(\"trj.dcd\",\"topology.xml\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And run the rdf calculation\n",
    "\n",
    "> rdf.py -i rdf.inp -np 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's compare resulting RDFs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rdf_repro_gromacs = MT.ReadRDF('../5-CGMD-GROMACS/result/NaCl_repro_gromacs.rdf', quiet=True)\n",
    "rdf_repro_lammps = MT.ReadRDF('../6-CGMD-LAMMPS/result/NaCl_repro_lammps.rdf', quiet=True)\n",
    "rdf_repro_galamost = MT.ReadRDF('NaCl_galamost_repro.rdf', quiet=True)\n",
    "rdf_ref = MT.ReadRDF('../3-RDF/NaCl_1M.rdf',quiet=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MT.MultPlot([rdf_ref, rdf_repro_galamost, rdf_repro_gromacs, rdf_repro_lammps])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Galamost shows somewhat worse agreement with the rest of RDF, which we can address to difference in potentials, caused by their truncation to 9A-cutoff"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
