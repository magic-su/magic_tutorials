# Tutorial for MagiC-2.4++ #
## Computation of solvent-mediated effective potentials between Na+ and Cl- ions in water ##
Alexander Lyubartsev(1), Alexander Mirzoev(2)
(1): Department of Materials and Environmental Chemistry, Stockholm University, Stockholm, Sweden (2): School of Biological Sciences, Nanyang Technological University, Singapore
November 2017
This tutorial illustrate example of computation of effective potentials for ions in NaCl solution. This example follows these works:
A.Lyubartsev, A.Laaksonen, "Calculation of Effective Interaction Potentials from Radial Distribution Functions: A Reverse Monte Carlo Approach", Phys.Rev.E, 52, 3730 (1995)
A.Mirzoev, A.P.Lyubartsev, "Effective solvent-mediated interaction potentials of Na+ and Cl- in aqueous solution: temperature dependence", PCCP, 13, 5722 (2011)
In the example, radial distribution functions (RDF) between ions in water solutions, computed by atomistic molecular dynamics, are used to compute effective solvent-mediated potentials, which, in simulation of ions without explicit water, reproduce the same RDF as those obtained in the atomistic simulations. The example contains 4 folders in which input and output files relevant for each step of computations of the effective potentials are collected:
1 - High-resolution (atomistic) molecular dynamics ( 1-HighResolutionMD )
2 - Coarse-graining of the trajectory ( 2-CGtraj )
3 - Computation of the radial distribution functions from the coarse-grained trajectory ( 3-RDF )
4 - Computations of the effective potentials ( 4-IMC ) Additionally, folder Input-only contains the same 4 subfolders with only input files. It can be advisable to run test study in this folder and then compare with the reference output files. It is assumed that MagiC package (v. >= 2.4) is compiled and installed.



To start the tutorial use jupyter notebook (former ipython notebook):

> jupyter notebook MagiC3-Tutorial-NaCL.ipynb

or

> ipython notebook MagiC3-Tutorial-NaCL.ipynb