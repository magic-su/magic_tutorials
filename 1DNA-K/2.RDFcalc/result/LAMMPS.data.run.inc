
 #PairIJ Coeffs

pair_coeff * * coul/long
pair_coeff 2 2 table D_D.table D_D
pair_coeff 1 2 table P_D.table P_D
pair_coeff 1 1 table P_P.table P_P

 #Angle Coeffs

angle_coeff 1  DNA.CG.4.table DNA.CG.4 
angle_coeff 2  DNA.CG.5.table DNA.CG.5 
angle_coeff 3  DNA.CG.6.table DNA.CG.6 

 #Bond Coeffs

bond_coeff 1  DNA.CG.1.table DNA.CG.1 
bond_coeff 2  DNA.CG.2.table DNA.CG.2 
bond_coeff 3  DNA.CG.3.table DNA.CG.3 
