
 #PairIJ Coeffs

pair_coeff * * coul/long
pair_coeff 1 1 table D_D.table D_D
pair_coeff 1 2 table D_P.table D_P
pair_coeff 1 3 table D_K.table D_K
pair_coeff 1 4 table D_CL.table D_CL
pair_coeff 2 2 table P_P.table P_P
pair_coeff 2 3 table P_K.table P_K
pair_coeff 2 4 table P_CL.table P_CL
pair_coeff 3 3 table K_K.table K_K
pair_coeff 3 4 table K_CL.table K_CL
pair_coeff 4 4 table CL_CL.table CL_CL

 #Angle Coeffs

angle_coeff 1 table  DNA.CG.4.table DNA.CG.4 
angle_coeff 2 table  DNA.CG.5.table DNA.CG.5 
angle_coeff 3 table  DNA.CG.6.table DNA.CG.6 

 #Bond Coeffs

bond_coeff 1 table  DNA.CG.1.table DNA.CG.1 
bond_coeff 2 table  DNA.CG.2.table DNA.CG.2 
bond_coeff 3 table  DNA.CG.3.table DNA.CG.3 
bond_coeff 4 zero 
