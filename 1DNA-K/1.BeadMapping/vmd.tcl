menu main on
mol modstyle 0 0 CPK 1.000000 0.300000 12.000000 12.000000
pbc set {151.0 151.0 151.0} -all
pbc box

mol color ColorID 0
mol representation CPK 1.000000 0.300000 12.000000 12.000000
mol selection name all
mol material Opaque
mol addrep 0
mol modselect 1 0 name \"D.*\"
mol modstyle 1 0 CPK 6.000000 0.300000 12.000000 12.000000
mol modcolor 1 0 ColorID 7

mol color Name
mol representation CPK 1.000000 0.300000 12.000000 12.000000
mol selection name \"P.*\"
mol material Opaque
mol addrep 0
mol modselect 2 0 name \"P.*\"
mol modstyle 2 0 CPK 3.000000 0.300000 12.000000 12.000000
mol modcolor 2 0 ColorID 1

mol color ColorID 2
mol representation CPK 1.000000 0.300000 12.000000 12.000000
mol selection name K
mol material Opaque
mol addrep 0
