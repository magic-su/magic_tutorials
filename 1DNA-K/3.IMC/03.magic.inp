! Block of system definition --------------------------------------------------------------------
 # Number of molecular types (species) preset in the system
 NMType = 3
 # Names of molecular types separated by comma: Name_1, Name_2, ... , Name_NMType (same names as used for the mcm-files, but without extension)
 NameMType = DNA.CG, K.CG, CL.CG
 # Number of molecules of each type, separated by comma
 NMolMType = 1, 343, 265
! If molecules of the molecular type are allowed to move. Default - all true
 LMoveMType = TRUE, T , T
 # Periodic cell sizes: X,Y,Z
 Box = 151.85, 151.85, 151.85
# Dielectric permittivity
 Epsilon = 78.0
# Temperature 
 TEMP=298.
# Non-standard exclusions for short-range interactions
 ExclusionSR = exclusions_SR.dat  
# ExclusionEL = exclusions_SR.dat  We use standard exclusions
! Block of Monte-Carlo parameters -------------------------------------------------------------
! Number of Monte-Carlo steps in single inverce iteration, including equilibration
  MCSteps = 50000000,
! Number of Monte-Carlo steps for equilibration
  MCStepsEquil = 25000000,
! Size of single atom displacement step (A)
  MCStepAtom = 1.0, 5.0, 5.0
! Size of  molecule translation displacement step (A)
  MCStepTransMol =  1.0 
! Size of  molecule rotation displacement step (rad)
  MCStepRotMol = 0.02
! How often to perform molecule translation step. Default: 0 - do not perform
  iMCStepTransMol = 300
! How often to perform molecule rotation step. Default: 0 - do not perform
  iMCStepRotMol = 300
! How many autoadjustments of MCsteps to perform during equilibration stage
!  NMCAutoAdjust=10
! How often to recalculate total energy and write output
  iCalcEnergy = 1000000
! Cut-off distance for real space part of  electrostatic interactions (A)
  RCutEl=25.0
! Evald Electrostatic interaction parameters: Default values AF=3.0, FQ=9.0
  AF = 2.6
  FQ = 8.0
! Level of high prohibiting potential, for points where reference RDF is zero. Default=1000 (kT) 
  ProhibPotLevel=1000.0
! RandomSeed number
  RandomSeed=511635

! Block of Inverse procedure parameters-------------------------------------------------------
! Which inverse mode to use? IBI (iterative Boltzmann Inversion), IMC (inverse Monte Carlo), NG (Newton Gauss optimization)
  InverseMode = IMC
! How many inverse iterations to perform
  NIter=6
! How often to calculate averages
  IAverage=500
! Regularization parameter - the potential corrections will be scaled by this factor
  REGP = 0.9,
! Maximum absolute change of the potential (in kT) - limit the correction
  MaxPotCor=2.0
! Use final structure of the previous inverse iteration as starting one for the following iteration.
  KeepStructure=False
! Parameter limiting relative differences between reference and resulting averages. Default: 10 times
  MaxRelDif=10.0
! How often to perform intermediate potential correction checks? Default:0 - do not perform.
  iPotCorCheck = 0

! Block of Input/Output parameters-------------------------------------------------------------
! Verbosity level (how much information to print in the output). Default: 5
  VerboseLevel=5
! Input file with Reference Distribution Functions
  InputRDF= 1DNA-K.rdf
! Input file with tabulated Potentials
  InputPot = 02.1DNA-K.i006.pot
! Input files with starting coordinates for the system (Base name for the set of coordinates input files). Default - no file.
#  InputStartCoords=1DNA-K.CG.xtc, 1000
! Input file with coordinates of frozen (LMoveMType=False) molecules. Default - no file
! Base-name for the output files
  Output = 03.1DNA-K
! Dump last configuration of each MC-simulation to an xmol-file. Default - No
  DumpLastConf = .false.
! How often to write configurations to the trajectory. Default 0 - do not write at all
  WriteTraj = 0
