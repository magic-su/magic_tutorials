#Initialization

units		real
atom_style	full
pair_style hybrid/overlay table linear 2501 coul/long 20.0
bond_style table linear 2501
angle_style table linear 2501
read_data LAMMPS.data
include LAMMPS.data.run.inc

# Settings
variable        t equal 297
dielectric 70.0 # 
kspace_style ewald 1.0e-5
special_bonds lj/coul 0.0 0.0 1.0 angle yes # specify exclusions (i.g. exclude all pairwise bonded or angle bonded atoms)
neigh_modify delay 5 every 1 check yes cluster no
neighbor 5.0 bin
thermo_style multi
thermo_modify lost error
timestep 0.1 # fs.
velocity  all create $t 12345 mom yes rot yes  # generate velocities

dump 1 all xtc 1000 trj.xtc
dump_modify 1 unwrap yes

# 1st equilibration run with direct velocity rescaling.
fix		1 all nve 
fix 		2 all temp/rescale 1 $t $t 0.05 1.0

thermo	        1
run             1000

velocity	all scale $t # scale velocities
unfix		2 # remove rescaling-thermostat

# 2nd equilibration run
fix		2 all langevin ${t} ${t} 1.0 59804 zero yes angmom no 
fix 		3 all momentum 100 linear 1 1 1 angular #rescale - remove COM movement
fix		Tavrg  all  ave/time 1 1000 1000 c_thermo_temp
thermo_style custom step etotal ke f_Tavrg temp pe ebond eangle edihed eimp evdwl ecoul elong press
thermo_modify lost error
thermo 1000 # print thermodynamic variables every 1000 steps
timestep 1
#run the simulation
run 10000

# Production run 
unfix Tavrg
unfix 2
fix		2 all langevin ${t} ${t} 100.0 69804 zero yes angmom no 
fix		Tavrg  all  ave/time 10 10000 100000 c_thermo_temp
thermo_style custom step etotal ke f_Tavrg temp pe ebond eangle edihed eimp evdwl ecoul elong press
thermo 1000000 # print thermodynamic variables every 100000 steps
timestep 5 
run 50000000
