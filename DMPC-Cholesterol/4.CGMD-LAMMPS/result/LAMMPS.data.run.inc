
 #PairIJ Coeffs

pair_coeff * * coul/long
pair_coeff 4 4 table COL_COL.table COL_COL
pair_coeff 4 7 table COL_CHB4.table COL_CHB4
pair_coeff 4 5 table COL_CLOH.table COL_CLOH
pair_coeff 4 6 table COL_CLRC.table COL_CLRC
pair_coeff 2 4 table PCL_COL.table PCL_COL
pair_coeff 2 2 table PCL_PCL.table PCL_PCL
pair_coeff 2 3 table PCL_CHA4.table PCL_CHA4
pair_coeff 2 7 table PCL_CHB4.table PCL_CHB4
pair_coeff 2 5 table PCL_CLOH.table PCL_CLOH
pair_coeff 2 6 table PCL_CLRC.table PCL_CLRC
pair_coeff 3 4 table CHA4_COL.table CHA4_COL
pair_coeff 3 3 table CHA4_CHA4.table CHA4_CHA4
pair_coeff 3 7 table CHA4_CHB4.table CHA4_CHB4
pair_coeff 3 5 table CHA4_CLOH.table CHA4_CLOH
pair_coeff 3 6 table CHA4_CLRC.table CHA4_CLRC
pair_coeff 1 4 table NCL_COL.table NCL_COL
pair_coeff 1 2 table NCL_PCL.table NCL_PCL
pair_coeff 1 3 table NCL_CHA4.table NCL_CHA4
pair_coeff 1 1 table NCL_NCL.table NCL_NCL
pair_coeff 1 7 table NCL_CHB4.table NCL_CHB4
pair_coeff 1 5 table NCL_CLOH.table NCL_CLOH
pair_coeff 1 6 table NCL_CLRC.table NCL_CLRC
pair_coeff 7 7 table CHB4_CHB4.table CHB4_CHB4
pair_coeff 5 7 table CLOH_CHB4.table CLOH_CHB4
pair_coeff 5 5 table CLOH_CLOH.table CLOH_CLOH
pair_coeff 5 6 table CLOH_CLRC.table CLOH_CLRC
pair_coeff 6 7 table CLRC_CHB4.table CLRC_CHB4
pair_coeff 6 6 table CLRC_CLRC.table CLRC_CLRC

 #Angle Coeffs

angle_coeff 1  DMPC.CG.6.table DMPC.CG.6 
angle_coeff 2  DMPC.CG.7.table DMPC.CG.7 
angle_coeff 3  DMPC.CG.8.table DMPC.CG.8 
angle_coeff 4  DMPC.CG.9.table DMPC.CG.9 
angle_coeff 5  DMPC.CG.10.table DMPC.CG.10 
angle_coeff 6  Chol.CG.5.table Chol.CG.5 
angle_coeff 7  Chol.CG.6.table Chol.CG.6 
angle_coeff 8  Chol.CG.7.table Chol.CG.7 

 #Bond Coeffs

bond_coeff 1  DMPC.CG.1.table DMPC.CG.1 
bond_coeff 2  DMPC.CG.2.table DMPC.CG.2 
bond_coeff 3  DMPC.CG.3.table DMPC.CG.3 
bond_coeff 4  DMPC.CG.4.table DMPC.CG.4 
bond_coeff 5  DMPC.CG.5.table DMPC.CG.5 
bond_coeff 6  Chol.CG.1.table Chol.CG.1 
bond_coeff 7  Chol.CG.2.table Chol.CG.2 
bond_coeff 8  Chol.CG.3.table Chol.CG.3 
bond_coeff 9  Chol.CG.4.table Chol.CG.4 
