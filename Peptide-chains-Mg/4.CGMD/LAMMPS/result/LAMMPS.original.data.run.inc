
 #PairIJ Coeffs

pair_coeff * * coul/long
pair_coeff 2 2 table POS_POS.table POS_POS
pair_coeff 2 3 table POS_POL.table POS_POL
pair_coeff 2 4 table POS_GLY.table POS_GLY
pair_coeff 2 5 table POS_NEG.table POS_NEG
pair_coeff 2 6 table POS_MG.table POS_MG
pair_coeff 2 7 table POS_NA.table POS_NA
pair_coeff 2 8 table POS_K.table POS_K
pair_coeff 2 9 table POS_CL.table POS_CL
pair_coeff 3 3 table POL_POL.table POL_POL
pair_coeff 3 4 table POL_GLY.table POL_GLY
pair_coeff 3 5 table POL_NEG.table POL_NEG
pair_coeff 3 6 table POL_MG.table POL_MG
pair_coeff 3 7 table POL_NA.table POL_NA
pair_coeff 3 8 table POL_K.table POL_K
pair_coeff 3 9 table POL_CL.table POL_CL
pair_coeff 1 2 table NPL_POS.table NPL_POS
pair_coeff 1 3 table NPL_POL.table NPL_POL
pair_coeff 1 1 table NPL_NPL.table NPL_NPL
pair_coeff 1 4 table NPL_GLY.table NPL_GLY
pair_coeff 1 5 table NPL_NEG.table NPL_NEG
pair_coeff 1 6 table NPL_MG.table NPL_MG
pair_coeff 1 7 table NPL_NA.table NPL_NA
pair_coeff 1 8 table NPL_K.table NPL_K
pair_coeff 1 9 table NPL_CL.table NPL_CL
pair_coeff 4 4 table GLY_GLY.table GLY_GLY
pair_coeff 4 5 table GLY_NEG.table GLY_NEG
pair_coeff 4 6 table GLY_MG.table GLY_MG
pair_coeff 4 7 table GLY_NA.table GLY_NA
pair_coeff 4 8 table GLY_K.table GLY_K
pair_coeff 4 9 table GLY_CL.table GLY_CL
pair_coeff 5 5 table NEG_NEG.table NEG_NEG
pair_coeff 5 6 table NEG_MG.table NEG_MG
pair_coeff 5 7 table NEG_NA.table NEG_NA
pair_coeff 5 8 table NEG_K.table NEG_K
pair_coeff 5 9 table NEG_CL.table NEG_CL
pair_coeff 6 6 table MG_MG.table MG_MG
pair_coeff 6 7 table MG_NA.table MG_NA
pair_coeff 6 8 table MG_K.table MG_K
pair_coeff 6 9 table MG_CL.table MG_CL
pair_coeff 7 7 table NA_NA.table NA_NA
pair_coeff 7 8 table NA_K.table NA_K
pair_coeff 7 9 table NA_CL.table NA_CL
pair_coeff 8 8 table K_K.table K_K
pair_coeff 8 9 table K_CL.table K_CL
pair_coeff 9 9 table CL_CL.table CL_CL

 #Angle Coeffs

angle_coeff 1  frgm_1.CG.2.table frgm_1.CG.2 
angle_coeff 2  frgm_2.CG.2.table frgm_2.CG.2 
angle_coeff 3  frgm_2.CG.3.table frgm_2.CG.3 
angle_coeff 4  frgm_3.CG.2.table frgm_3.CG.2 
angle_coeff 5  frgm_3.CG.3.table frgm_3.CG.3 
angle_coeff 6  frgm_3.CG.4.table frgm_3.CG.4 
angle_coeff 7  frgm_4.CG.2.table frgm_4.CG.2 
angle_coeff 8  frgm_5.CG.2.table frgm_5.CG.2 
angle_coeff 9  frgm_5.CG.3.table frgm_5.CG.3 
angle_coeff 10  frgm_6.CG.2.table frgm_6.CG.2 
angle_coeff 11  frgm_6.CG.3.table frgm_6.CG.3 
angle_coeff 12  frgm_7.CG.2.table frgm_7.CG.2 
angle_coeff 13  frgm_8.CG.2.table frgm_8.CG.2 
angle_coeff 14  frgm_9.CG.2.table frgm_9.CG.2 
angle_coeff 15  frgm_9.CG.3.table frgm_9.CG.3 
angle_coeff 16  frgm_10.CG.2.table frgm_10.CG.2 
angle_coeff 17  frgm_10.CG.3.table frgm_10.CG.3 
angle_coeff 18  frgm_10.CG.4.table frgm_10.CG.4 
angle_coeff 19  frgm_11.CG.2.table frgm_11.CG.2 
angle_coeff 20  frgm_11.CG.3.table frgm_11.CG.3 
angle_coeff 21  frgm_12.CG.2.table frgm_12.CG.2 
angle_coeff 22  frgm_12.CG.3.table frgm_12.CG.3 
angle_coeff 23  frgm_13.CG.2.table frgm_13.CG.2 
angle_coeff 24  frgm_14.CG.2.table frgm_14.CG.2 
angle_coeff 25  frgm_15.CG.2.table frgm_15.CG.2 
angle_coeff 26  frgm_16.CG.2.table frgm_16.CG.2 
angle_coeff 27  frgm_17.CG.2.table frgm_17.CG.2 
angle_coeff 28  frgm_17.CG.3.table frgm_17.CG.3 
angle_coeff 29  frgm_18.CG.2.table frgm_18.CG.2 
angle_coeff 30  frgm_18.CG.3.table frgm_18.CG.3 
angle_coeff 31  frgm_19.CG.2.table frgm_19.CG.2 
angle_coeff 32  frgm_20.CG.2.table frgm_20.CG.2 
angle_coeff 33  frgm_20.CG.3.table frgm_20.CG.3 
angle_coeff 34  frgm_21.CG.2.table frgm_21.CG.2 
angle_coeff 35  frgm_21.CG.3.table frgm_21.CG.3 
angle_coeff 36  frgm_22.CG.2.table frgm_22.CG.2 
angle_coeff 37  frgm_22.CG.3.table frgm_22.CG.3 
angle_coeff 38  frgm_22.CG.4.table frgm_22.CG.4 
angle_coeff 39  frgm_23.CG.2.table frgm_23.CG.2 
angle_coeff 40  frgm_23.CG.3.table frgm_23.CG.3 
angle_coeff 41  frgm_24.CG.2.table frgm_24.CG.2 
angle_coeff 42  frgm_24.CG.3.table frgm_24.CG.3 
angle_coeff 43  frgm_25.CG.2.table frgm_25.CG.2 
angle_coeff 44  frgm_25.CG.3.table frgm_25.CG.3 
angle_coeff 45  frgm_26.CG.2.table frgm_26.CG.2 
angle_coeff 46  frgm_27.CG.2.table frgm_27.CG.2 
angle_coeff 47  frgm_28.CG.2.table frgm_28.CG.2 
angle_coeff 48  frgm_29.CG.2.table frgm_29.CG.2 
angle_coeff 49  frgm_29.CG.3.table frgm_29.CG.3 
angle_coeff 50  frgm_30.CG.2.table frgm_30.CG.2 
angle_coeff 51  frgm_31.CG.2.table frgm_31.CG.2 
angle_coeff 52  frgm_31.CG.3.table frgm_31.CG.3 
angle_coeff 53  frgm_32.CG.2.table frgm_32.CG.2 
angle_coeff 54  frgm_33.CG.2.table frgm_33.CG.2 
angle_coeff 55  frgm_33.CG.3.table frgm_33.CG.3 
angle_coeff 56  frgm_34.CG.2.table frgm_34.CG.2 
angle_coeff 57  frgm_34.CG.3.table frgm_34.CG.3 
angle_coeff 58  frgm_35.CG.2.table frgm_35.CG.2 
angle_coeff 59  frgm_35.CG.3.table frgm_35.CG.3 
angle_coeff 60  frgm_36.CG.2.table frgm_36.CG.2 
angle_coeff 61  frgm_36.CG.3.table frgm_36.CG.3 
angle_coeff 62  frgm_36.CG.4.table frgm_36.CG.4 
angle_coeff 63  frgm_37.CG.2.table frgm_37.CG.2 
angle_coeff 64  frgm_37.CG.3.table frgm_37.CG.3 
angle_coeff 65  frgm_38.CG.2.table frgm_38.CG.2 
angle_coeff 66  frgm_38.CG.3.table frgm_38.CG.3 
angle_coeff 67  frgm_39.CG.2.table frgm_39.CG.2 
angle_coeff 68  frgm_39.CG.3.table frgm_39.CG.3 
angle_coeff 69  frgm_40.CG.2.table frgm_40.CG.2 
angle_coeff 70  frgm_40.CG.3.table frgm_40.CG.3 
angle_coeff 71  frgm_41.CG.2.table frgm_41.CG.2 
angle_coeff 72  frgm_41.CG.3.table frgm_41.CG.3 
angle_coeff 73  frgm_42.CG.2.table frgm_42.CG.2 
angle_coeff 74  frgm_43.CG.2.table frgm_43.CG.2 
angle_coeff 75  frgm_44.CG.2.table frgm_44.CG.2 
angle_coeff 76  frgm_45.CG.2.table frgm_45.CG.2 
angle_coeff 77  frgm_46.CG.2.table frgm_46.CG.2 
angle_coeff 78  frgm_46.CG.3.table frgm_46.CG.3 
angle_coeff 79  frgm_46.CG.4.table frgm_46.CG.4 
angle_coeff 80  frgm_47.CG.2.table frgm_47.CG.2 
angle_coeff 81  frgm_48.CG.2.table frgm_48.CG.2 
angle_coeff 82  frgm_48.CG.3.table frgm_48.CG.3 

 #Bond Coeffs

bond_coeff 1  frgm_1.CG.1.table frgm_1.CG.1 
bond_coeff 2  frgm_2.CG.1.table frgm_2.CG.1 
bond_coeff 3  frgm_3.CG.1.table frgm_3.CG.1 
bond_coeff 4  frgm_4.CG.1.table frgm_4.CG.1 
bond_coeff 5  frgm_5.CG.1.table frgm_5.CG.1 
bond_coeff 6  frgm_6.CG.1.table frgm_6.CG.1 
bond_coeff 7  frgm_7.CG.1.table frgm_7.CG.1 
bond_coeff 8  frgm_8.CG.1.table frgm_8.CG.1 
bond_coeff 9  frgm_9.CG.1.table frgm_9.CG.1 
bond_coeff 10  frgm_10.CG.1.table frgm_10.CG.1 
bond_coeff 11  frgm_11.CG.1.table frgm_11.CG.1 
bond_coeff 12  frgm_12.CG.1.table frgm_12.CG.1 
bond_coeff 13  frgm_13.CG.1.table frgm_13.CG.1 
bond_coeff 14  frgm_14.CG.1.table frgm_14.CG.1 
bond_coeff 15  frgm_15.CG.1.table frgm_15.CG.1 
bond_coeff 16  frgm_16.CG.1.table frgm_16.CG.1 
bond_coeff 17  frgm_17.CG.1.table frgm_17.CG.1 
bond_coeff 18  frgm_18.CG.1.table frgm_18.CG.1 
bond_coeff 19  frgm_19.CG.1.table frgm_19.CG.1 
bond_coeff 20  frgm_20.CG.1.table frgm_20.CG.1 
bond_coeff 21  frgm_21.CG.1.table frgm_21.CG.1 
bond_coeff 22  frgm_22.CG.1.table frgm_22.CG.1 
bond_coeff 23  frgm_23.CG.1.table frgm_23.CG.1 
bond_coeff 24  frgm_24.CG.1.table frgm_24.CG.1 
bond_coeff 25  frgm_25.CG.1.table frgm_25.CG.1 
bond_coeff 26  frgm_26.CG.1.table frgm_26.CG.1 
bond_coeff 27  frgm_27.CG.1.table frgm_27.CG.1 
bond_coeff 28  frgm_28.CG.1.table frgm_28.CG.1 
bond_coeff 29  frgm_29.CG.1.table frgm_29.CG.1 
bond_coeff 30  frgm_30.CG.1.table frgm_30.CG.1 
bond_coeff 31  frgm_31.CG.1.table frgm_31.CG.1 
bond_coeff 32  frgm_32.CG.1.table frgm_32.CG.1 
bond_coeff 33  frgm_33.CG.1.table frgm_33.CG.1 
bond_coeff 34  frgm_34.CG.1.table frgm_34.CG.1 
bond_coeff 35  frgm_35.CG.1.table frgm_35.CG.1 
bond_coeff 36  frgm_36.CG.1.table frgm_36.CG.1 
bond_coeff 37  frgm_37.CG.1.table frgm_37.CG.1 
bond_coeff 38  frgm_38.CG.1.table frgm_38.CG.1 
bond_coeff 39  frgm_39.CG.1.table frgm_39.CG.1 
bond_coeff 40  frgm_40.CG.1.table frgm_40.CG.1 
bond_coeff 41  frgm_41.CG.1.table frgm_41.CG.1 
bond_coeff 42  frgm_42.CG.1.table frgm_42.CG.1 
bond_coeff 43  frgm_43.CG.1.table frgm_43.CG.1 
bond_coeff 44  frgm_44.CG.1.table frgm_44.CG.1 
bond_coeff 45  frgm_45.CG.1.table frgm_45.CG.1 
bond_coeff 46  frgm_46.CG.1.table frgm_46.CG.1 
bond_coeff 47  frgm_47.CG.1.table frgm_47.CG.1 
bond_coeff 48  frgm_48.CG.1.table frgm_48.CG.1 
