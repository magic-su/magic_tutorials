
 #PairIJ Coeffs

pair_coeff * * coul/long
pair_coeff 2 2 table POS_POS.table POS_POS
pair_coeff 2 3 table POS_POL.table POS_POL
pair_coeff 2 4 table POS_GLY.table POS_GLY
pair_coeff 2 5 table POS_NEG.table POS_NEG
pair_coeff 2 6 table POS_MG.table POS_MG
pair_coeff 2 7 table POS_NA.table POS_NA
pair_coeff 2 8 table POS_K.table POS_K
pair_coeff 2 9 table POS_CL.table POS_CL
pair_coeff 3 3 table POL_POL.table POL_POL
pair_coeff 3 4 table POL_GLY.table POL_GLY
pair_coeff 3 5 table POL_NEG.table POL_NEG
pair_coeff 3 6 table POL_MG.table POL_MG
pair_coeff 3 7 table POL_NA.table POL_NA
pair_coeff 3 8 table POL_K.table POL_K
pair_coeff 3 9 table POL_CL.table POL_CL
pair_coeff 1 2 table NPL_POS.table NPL_POS
pair_coeff 1 3 table NPL_POL.table NPL_POL
pair_coeff 1 1 table NPL_NPL.table NPL_NPL
pair_coeff 1 4 table NPL_GLY.table NPL_GLY
pair_coeff 1 5 table NPL_NEG.table NPL_NEG
pair_coeff 1 6 table NPL_MG.table NPL_MG
pair_coeff 1 7 table NPL_NA.table NPL_NA
pair_coeff 1 8 table NPL_K.table NPL_K
pair_coeff 1 9 table NPL_CL.table NPL_CL
pair_coeff 4 4 table GLY_GLY.table GLY_GLY
pair_coeff 4 5 table GLY_NEG.table GLY_NEG
pair_coeff 4 6 table GLY_MG.table GLY_MG
pair_coeff 4 7 table GLY_NA.table GLY_NA
pair_coeff 4 8 table GLY_K.table GLY_K
pair_coeff 4 9 table GLY_CL.table GLY_CL
pair_coeff 5 5 table NEG_NEG.table NEG_NEG
pair_coeff 5 6 table NEG_MG.table NEG_MG
pair_coeff 5 7 table NEG_NA.table NEG_NA
pair_coeff 5 8 table NEG_K.table NEG_K
pair_coeff 5 9 table NEG_CL.table NEG_CL
pair_coeff 6 6 table MG_MG.table MG_MG
pair_coeff 6 7 table MG_NA.table MG_NA
pair_coeff 6 8 table MG_K.table MG_K
pair_coeff 6 9 table MG_CL.table MG_CL
pair_coeff 7 7 table NA_NA.table NA_NA
pair_coeff 7 8 table NA_K.table NA_K
pair_coeff 7 9 table NA_CL.table NA_CL
pair_coeff 8 8 table K_K.table K_K
pair_coeff 8 9 table K_CL.table K_CL
pair_coeff 9 9 table CL_CL.table CL_CL

 #Angle Coeffs

angle_coeff 1  frgm_3.CG.2.table frgm_3.CG.2 
angle_coeff 2  frgm_3.CG.3.table frgm_3.CG.3 
angle_coeff 3  frgm_3.CG.4.table frgm_3.CG.4 

 #Bond Coeffs

bond_coeff 1  frgm_1.CG.1.table frgm_1.CG.1 
